module.exports = {
  siteTitle: 'earnest.ly',
  siteDescription: "earnest.ly",
  authorName: 'Blake Wesley Thomas',
  twitterUsername: 'dijjnn',
  authorAvatar: 'avatar.jpg', // file in content/images
  multilangPosts: false, // enable/disable flags in post lists
  authorDescription: `
    Jovial introvert, business mohawk inventor, goose right-of-way denier.
    Home with <a href="https://twitter.com/cmsthomas">@cmsthomas</a> &
    <a href="https://twitter.com/madlilmax">@madlilmax</a>, work with
    <a href="https://twitter.com/noredink">@noredink</a>.
    <a href="http://pronoun.is/he">pronoun.is/he</a>
  `,
  siteUrl: 'https://earnest.ly/',
  disqusSiteUrl: 'https://dijjnn.disqus.com/',
  // Prefixes all links.
  pathPrefix: '/', // Note: it must *not* have a trailing slash.
  siteCover: 'cover.jpg', // file in content/images
  googleAnalyticsId: 'UA-67868977-1',
  background_color: '#ffffff',
  theme_color: '#222222',
  display: 'standalone',
  //icon: 'content/images/gatsby-icon.png',
  icon: 'content/images/e.png',
  postsPerPage: 5,
  disqusShortname: 'dijjnn',
  headerLinks: [
    {
      label: 'Home 🏡',
      url: '/',
    },
    {
      label: 'About 👤',
      url: '/about',
    },
  ],
  // Footer information (ex: Github, Netlify...)
  websiteHost: {
    name: 'GitHub',
    url: 'https://github.com',
  },
  footerLinks: [
    {
      sectionName: 'Explore',
      links: [
        {
          label: 'About',
          url: '/about',
        },
      ],
    },
    {
      sectionName: 'Follow the author',
      links: [
        {
          label: 'Github',
          url: 'https://github.com/bwthomas',
        },
        {
          label: 'Twitter',
          url: 'https://twitter.com/dijjnn',
        },
      ],
    },
  ],
}
