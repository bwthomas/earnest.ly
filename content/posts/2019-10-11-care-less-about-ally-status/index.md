---
title: "I Couldn't Care Less if You Think I'm an Ally"
slug: care-less-about-ally-status
cover: ../../images/cover.jpg
generate-card: false
date: 2019-10-11
language: en
tags:
    - Diversity & Inclusion
---

I sat in the ballroom of a Santa Cruz resort listening with a quiet mind,
carefully if not intently. In place of the exuberance of the previous day, there
was a muted, relaxed feel to the room. Eighty of my coworkers sat languidly as
Pam, the facilitator, came to a point in the session where she invited anyone
willing to share personal stories connecting them to our mission of helping
students become better writers. I usually try to avoid compulsively filling
silence, but on this occasion I allowed myself to rationalize my volunteerism by
believing my participation would encourage others to share. Wanting to model
vulnerability, I had in mind to share a story from my childhood. As I moved from
raised hand to standing in preparedness to speak, Pam began to make her way
towards me, skirting tables and dodging backpacks. I experienced a brief moment
of confusion, unsure what her purpose was. She thankfully clarified her intent
by saying "let me give you my mic," which I then realized she had uncoiled from
around her ear where previously it had served its purpose quite unobtrusively.
In quick succession I felt mild chagrin at having been an inconvenience, then a
companion wave of relief as I reinterpreted her statement as an offer I might
decline. Ready to get on with it and say my bit I glibly replied "that's ok, I
can project."

My confusion returned when Pam ceased the extension of the hand holding the mic
in my direction and, pausing, began to speak slowly and thoughtfully into the
mic. "You know, I recently learned something that—you can speak loudly or," as
she gestured to me, "project your voice, but do you know, folks in the back of
the room may still have to strain to hear you. Or people with trouble hearing
from one ear or the other may not be able to hear you as well, even if they've
intentionally sat closer to the speaker. So, it's not about being able to
project, it's an accessibility issue. We're using the mic not just to speak
loudly, but to be more accessible."

Embarrassment washed over me. Feeling somewhat disoriented, I accepted the mic as

Pam offered it again and stumbled over my words for a few sentences before
regaining my composure, finishing my story and numbly returned the mic.  A few
minutes more, soothed by the heartfelt stories of others, I was able to return
to the calmly focused reverie of the day. Later that afternoon I would reflect
on this experience and begin to form the thoughts I'm now writing.

<br /><hr /><br />

It is not an easy thing to describe with precise language the characteristics
that define an ally. The idea is obscured in equal parts complexity, difficulty,
nuance and varying opinion. For those new to this concept, I'll loosely suggest
that an ally is someone who supports the cause of an oppressed or marginalized
group of which they are not a member. There's a good writeup on
[Dictionary.com](https://www.dictionary.com/e/what-is-an-ally/), and
organizations like GLAAD and others offer
[resources](https://www.glaad.org/resources/ally). Still, even with definitions
and guidance, it's easy to see why the idea can be hard to get a good handle on.
What kind of support does an ally provide, and who judges its merit? For which
causes exactly? Should an ally listen or speak, amplify others or be outspoken
themselves? If I marched for civil rights in 1963, am I a de facto ally in 2019?
As an ally, am I always "on?" What happens if I disagree with someone for whom
I'm supposed to be an ally?

Most of all, what happens when I make a mistake?

### No Bones

Having spent a lot of time observing and contemplating human behavior, I've
found humans possess a nearly universal motivation to view themselves as good.
As you learn more about the world, you add to our understanding of what good
means and occasionally alter your behavior slightly to meet that definition. You
learn that single-use plastics take a huge toll on the environment, so you might
come to see good people as those that try to actively reduce their use, and so
you stop using straws. When you learned about systemic oppression—ableism,
racism, misogyny, so on—you may come to believe that good people don't behave
with overt prejudice, and so you try to avoid prejudicial behavior. You might
even expand your understanding horizontally and connect awareness of other
struggles for equal rights to your understanding of _good_.

A time comes when someone lets you know that something you did or said failed to
meet your understanding of good. Maybe it's a word with a derogatory etymology.
Perhaps it's a belief about politics or policy. Your first reaction is
defensive, "But, I'm not a \_\_ist!" you clamor, "[I don't have a \_\_ist bone
in my body!](https://twitter.com/realDonaldTrump/status/1151129281134768128)"
Maybe that's enough to close the conversation, or maybe they persist by
explaining that regardless of your perception, this word, belief or behavior is
born of systemic oppression. How do you respond?

The catalog of of human emotion is extensive but there are a few phyla that
encompass most of the reactions one might expect, the most common of which is a
brief negative emotion—shame, embarrassment, guilt—followed by a conditioned
response to that negative emotion. Many people share an instinctual response to
a perceived source of negative emotion, which is to feel anger in the direction
of the source. Feel sad, and whoever or whatever "made me sad" stirs our ire.
Feelings of guilt or embarrassment ricochet through our emotions and reflect back
as indignation—"how DARE you make me feel like that!"

It turns out there's a reason this reaction is so common, and it boils down to a
defense mechanism to protect our personality. Upon feeling some existential
anxiety, our [anger arises in
defense](https://www.psychologytoday.com/us/blog/evolution-the-self/200807/what-your-anger-may-be-hiding)
of our identity, in this case our chosen identity as an ally. But then, why do
we care so much?

### Us vs. Them

One of the most difficult rational leaps we must make as we learn about ableism
or any other form of oppression is the realization that as much as we may want
to point at someone who's responsible, there's no one to point at so much as
everyone. Just as there are vocal, adamant and hateful people who speak openly
and brand themselves as bigoted, there are many, many more who tacitly endorse
oppression by accepting and even expecting the ways in which it benefits them as
"just the world we live in." In fact, we are as much a part of that system as
any other, having been inundated by it from childhood.

This runs afoul of our most primitive,
[instinctual](https://en.wikipedia.org/wiki/Ingroups_and_outgroups) need to
distinguish between that which must be defended and that which we expect to
attack us. As much as we might want a foe to fight, that foe is as much
ourselves as other.

The other side of this instinct is just as brutal, if not more so. If we allow
ourselves to follow our baser emotions we may find ourselves rejecting the very
community we had hoped to engage. "I'm came here to help, but if you don't want
me then fine, to hell with y'all anyway." The antedote to this is to realize
that none of this is about you as an individual, that you are not the center of
the story, and to realize that the sides you imagined aren't that way.

The more attached to your status as a member of some community that you are, the
more central it is to your identity, the more instinctual you will be about
defending that status from perceived attacks, up to & including by rejecting any
individual that calls you out, or even by ultimately rejecting the community as
not valuing your participation.

### Privilege

But again, it never should have been about you. This is the essence of
privilege, the unearned empowerment to prioritize our own interests and center
our own stories. Like oppression, privilege is also layered, and so we may
experience different degrees of privilege.

Regardless, we should put that privilege to work. Use whatever privilege you
have to raise the voices of others. Focus first on doing the work, and learn to
care less if you're thought of as an ally.

<br />

